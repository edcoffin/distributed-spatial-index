package org.coffin.spatialindex.gindex;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.coffin.spatialindex.Envelope;
import org.coffin.spatialindex.GIndexGrpc;
import org.coffin.spatialindex.IndexResponse;
import org.coffin.spatialindex.lib.ContainerHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author eric.coffin@unb.ca
 * Server receives queries, looks up feature in index, gets URL of matching
 * RIndex server and forwards request via the RIndexClient.
 * Implemented on gRPC (for the proto file see src/proto/spatial_index.proto).
 */

public class GIndexServer {

    private static final Logger logger = LoggerFactory.getLogger(GIndexServer.class);
    private static Map<String, RIndexClient> clientMap;

    private static String[] regionKeys = {
            "r-index-us-ny-manhattan-1", // us-ny-manhattan -- change to localhost for testing locally.
    };

    private static GIndex index;

    private Server server;

    private void start() throws IOException {
        // The port on which the server should run
        int port = 50051;
        server = ServerBuilder.forPort(port)
                .addService(new GIndexImpl())
                .build()
                .start();

        logger.info("GIndex Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                GIndexServer.this.stop();
                System.err.println("*** server shut down");
            }
        });

    }


    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    /**
     * Task runner.
     * First argument must be path to shape file.
     */
    public static void main(String[] args) throws Exception {

        new ContainerHelper().logRuntimeSpecs(logger);

        index = new GIndex();

        final GIndexServer server = new GIndexServer();
        logger.info("Running RIndexClient");
        clientMap = new HashMap<>();

        /*
         * For each region key, create a client
         * add it to the clientMap, query the envelope
         * and add it to the GIndex.
         * Later when queries are made to the GIndexServer::performLookup
         * we will get the region key from the GIndex (rtree),
         * and get the corresponding RIndexClient to forward the request to.
         */

        int servicePort = 50052;

        for (String regionKey : regionKeys) {
            logger.info("Add r-index @ {} {}", regionKey, servicePort);
            RIndexClient client = new RIndexClient(regionKey, servicePort);
            clientMap.put(regionKey, client);
            index.insert(client.getEnvelope(), regionKey);
        }

        server.start();
        server.blockUntilShutdown();

    }

    static class GIndexImpl extends GIndexGrpc.GIndexImplBase {
        @Override
        public void performLookup(Envelope request, StreamObserver<IndexResponse> responseObserver) {

            List<String> regionKeys = index.lookup(request);
            if (regionKeys.size() > 0) {
                IndexResponse response = clientMap.get(regionKeys.get(0)).performLookup(request);
                responseObserver.onNext(response);
                responseObserver.onCompleted();
            } else {
                responseObserver.onNext(IndexResponse.getDefaultInstance());
                responseObserver.onCompleted();
            }

        }
    }


}
