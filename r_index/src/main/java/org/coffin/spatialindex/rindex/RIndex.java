package org.coffin.spatialindex.rindex;

import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.index.strtree.STRtree;
import org.opengis.feature.simple.SimpleFeature;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

/**
 * @author eric.coffin@unb.ca
 *
 * Region RIndex
 *
 * Maintains an in-memory RIndex of a set of features.
 */
public class RIndex {

    private File file;
    private STRtree rTree;
    private Envelope envelope;

    public RIndex(File file) {
        this.file = file;
    }

    /**
     * Call after constructed to populate the index.
     */
    public void load() throws IOException {

        FileDataStore store = FileDataStoreFinder.getDataStore(file);
        SimpleFeatureSource featureSource = store.getFeatureSource();
        envelope = featureSource.getFeatures().getBounds();
        SimpleFeatureIterator iter = featureSource.getFeatures().features();

        this.rTree = new STRtree();

        int counter = 0;

        Instant loadStart = Instant.now();

        try {
            while (iter.hasNext()) {
                SimpleFeature f = iter.next();
                String sid = f.getID();
                MultiPolygon geom = (MultiPolygon) f.getDefaultGeometry();
                Envelope e = geom.getEnvelopeInternal();
                geom.setUserData(sid);
                rTree.insert(e, geom);
                counter++;
            }
        } finally {
            iter.close();
            store.dispose();
        }

        System.out.printf("%d features indexed in %d ms\n",
                counter,
                Duration.between(loadStart, Instant.now()).toMillis());

    }

    /**
     * Return the tree index.
     */
    public STRtree getRtree() {
        return rTree;
    }

    /**
     * Return the envelope of the entire index.
     */
    public org.coffin.spatialindex.Envelope getEnvelope() {
        return org.coffin.spatialindex.Envelope
                .newBuilder()
                .setMinX(this.envelope.getMinX())
                .setMinY(this.envelope.getMinY())
                .setMaxX(this.envelope.getMaxX())
                .setMaxY(this.envelope.getMaxY())
                .build();
    }
}