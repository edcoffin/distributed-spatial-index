
package org.coffin.spatialindex.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.coffin.spatialindex.Envelope;
import org.coffin.spatialindex.GIndexGrpc;
import org.coffin.spatialindex.IndexResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.Random;
import java.util.concurrent.TimeUnit;


/**
 * @author eric.coffin@unb.ca
 * Connect to and test the GIndex service.
 * Sends 10,000 queries.
 */

public class IndexClient {
    private static final Logger logger = LoggerFactory.getLogger(IndexClient.class);

    private final ManagedChannel channel;
    private final GIndexGrpc.GIndexBlockingStub blockingStub;

    /**
     * Construct org.coffin.spatialindex.client connecting to HelloWorld server at {@code host:port}.
     */
    public IndexClient(String host, int port) {

        this(ManagedChannelBuilder.forAddress(host, port)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext()
                .build());
    }

    /**
     * Construct org.coffin.spatialindex.client for accessing HelloWorld server using the existing channel.
     */
    IndexClient(ManagedChannel channel) {
        this.channel = channel;
        blockingStub = GIndexGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public void performLookup(Envelope request) {
        IndexResponse indexResponse;
        indexResponse = blockingStub.performLookup(request);
        logger.info("Returned features: {}", indexResponse.getIdList().size());
    }


    /**
     *
     */
    public static void main(String[] args) throws Exception {

        IndexClient client = new IndexClient("35.222.65.64", 50051);
        try {
            /* Access a service running on the local machine on port 50051 */
            EnvelopeList el = new EnvelopeList();
            el.populate();
            int counter = 0;
            Instant qStart = Instant.now();
            while (counter < 10000) {
                client.performLookup(el.getRandomEnvelope());
                counter++;
            }
            System.out.printf("%d features queried in %d ms\n",
                    counter,
                    Duration.between(qStart, Instant.now()).toMillis());

        } finally {
            client.shutdown();
        }
    }
}
