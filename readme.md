# Distributed Spatial Index

A distributed, in-memory spatial-index built to run on Kubernetes at cloud-scale.

Please see ecoffin_distributed_spatial_index.pdf for details on the project.

## Building

You must have Java 8 and Maven installed.

```
cd r_index
mvn install
mvn compile
mvn package # packages 3 jars
```

The following Artifacts will be created in /r_index/deploy/

- client\IndexClient.jar
- g-index\GIndex.jar
- r-index\RIndex.jar

## Running

Running RIndex.jar requires a command line argument pointing to a shape file. The test data for NYC building envelopes
is provided in /shape_files. You can use this path to run the RIndex.

Running GIndex simply requires RIndex to be running. If running locally, the GIndexServer must be modified to point to localhost:
https://bitbucket.org/edcoffin/distributed-spatial-index/src/60fc749ba0405562515720575d07765fec562c01/r_index/src/main/java/org/coffin/spatialindex/gindex/GIndexServer.java#lines-25

Once these two programs are running, you can then run the IndexClient. Once again for local testing you will need to modify
the class to point to localhost. In the future this address and the previous address mentioned should be externalized rather than hard-coded.

## Deploying

You will find Dockerfiles both the GIndex and the RIndex in /r_index/deploy/ .

Use these to build docker containers.

## Kubernetes
Once the containers are built, tagged and pushed to container repositories you can run them in the cloud.

Note that the hard-coded URLs listed above will need to be modified! A definite code-smell.

You will find v1 deploment and service files that can be applied to running cluster via kubectl.

These are located in /r_index/deploy/r-index/k8s, and /r_index/deploy/g-index/k8s.

To enable HTTP2 load balancing of requests from the GIndex to RIndex pods, you will need to install a gRPC/HTTP2 aware service mesh. LinkerD is a good choice for this.
